#!/bin/bash

if [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    export HTDOCS=../
else
    export PATH=$PATH:/cygdrive/c/devkitPro/devkitPPC/bin:/cygdrive/c/Python32
    export HTDOCS=../
fi

cd build
./build.sh $1 $2
