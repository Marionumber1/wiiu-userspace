#define s32 int
#define u32 unsigned int
#define f32 float
#define s16 short
#define u16 unsigned short
#define f16 short float
#define s8 char
#define u8 unsigned char
 
#define BOOL _Bool
#define TRUE 1
#define FALSE 0
#define NULL 0
