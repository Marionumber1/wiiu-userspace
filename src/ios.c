#include "coreinit.h"

void start()
{
  /* Load a good stack */
  asm(
      "lis %r1, 0x1ab5 ;"
      "ori %r1, %r1, 0xd138 ;"
  );

  /* Get a handle to coreinit.rpl */
  unsigned int coreinit_handle;
  OSDynLoad_Acquire("coreinit.rpl", &coreinit_handle);

  /* IOS functions */
  int (*IOS_Open)(unsigned char *path, int mode);

  /* Read the address of OSSetExceptionCallback() */
  OSDynLoad_FindExport(coreinit_handle, 0, "IOS_Open", &IOS_Open);

  /* Tests */
  int slc = IOS_Open("/dev/slc",1);
  int mlc = IOS_Open("/dev/mlc",1);
  int wfs = IOS_Open("/dev/wfs",1);
  int di = IOS_Open("/dev/di",1);
  int atfs = IOS_Open("/dev/atfs",1);
  int mcp = IOS_Open("/dev/mcp",1);
  int crypto = IOS_Open("/dev/crypto",1);
  int drh = IOS_Open("/dev/drh",1);
  int usbdrh = IOS_Open("/dev/usb/drh",1);
  int slot0 = IOS_Open("/dev/fat",1);

  char buffer[256];
  __os_snprintf(buffer, 256, "/dev/slc: %d\n/dev/mlc: %d\n/dev/wfs: %d\n/dev/di: %d\n/dev/atfs: %d\n/dev/mcp: %d\n/dev/crypto: %d\n/dev/drh: %d\n/dev/usb/drh: %d\n/dev/fat: %d", slc, mlc, wfs, di, atfs, mcp, crypto, drh, usbdrh, slot0);
  OSFatal(buffer);
}
