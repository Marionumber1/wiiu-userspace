.globl UhsSubmitBulkRequest
.extern memset
.extern IOS_Ioctlv

UhsSubmitBulkRequest:
                stwu      r1, -0x4F0(r1)
                stmw      r23, 0x4CC(r1)
                mfspr   r0, LR
                mr.       r25, r3
                mr        r26, r4
                mr        r27, r5
                mr        r28, r6
                mr        r30, r8
                mr        r31, r9
                stw       r0, 0x4F4(r1)
                mr        r29, r7
                beq       label4
                cmpwi     r29, 0
                beq       label4
                lwz       r12, 0(r25)
                cmpwi     r12, 2
                bne       label3
                addi      r0, r1, 0x47
                li        r4, 0
                clrrwi    r23, r0, 6
                li        r5, 0x454
                mr        r3, r23
                addi      r24, r23, 0x80
                bl        memset
                stw       r30, 0x91(r23)
                li        r0, 0xA1
                stw       r31, 0x85(r23)
                li        r8, 0xE
                stw       r28, 0x8D(r23)
                addi      r12, r23, 0xC
                stw       r8, 0x440(r23)
                cmpwi     r28, 1
                stw       r0, 4(r23)
                stw       r26, 0x80(r23)
                li        r9, 3
                stw       r24, 0(r23)
                stw       r9, 0x89(r23)
                stb       r27, 0x84(r23)
                bne       label1
                stw       r29, 0(r12)
                stw       r30, 4(r12)
                addi      r12, r12, 0xC

label1:			cmpwi     r28, 2
                mr        r11, r12
                bne       label2
                stw       r29, 0(r11)
                stw       r30, 4(r11)
                addi      r11, r11, 0xC

label2:			lis       r8, 0x2AAB
                subf      r0, r12, r11
                addi      r8, r8, -0x5555
                mulhw     r6, r8, r0
                subf      r9, r23, r12
                srawi     r7, r0, 0x1F
                mulhw     r10, r8, r9
                srawi     r6, r6, 1
                subf      r5, r7, r6
                srawi     r0, r9, 0x1F
                lwz       r3, 0xC(r25)
                srawi     r10, r10, 1
                mr        r7, r23
                li        r4, 0xE
                subf      r6, r0, r10
                bl        IOS_Ioctlv
                b         ret

label3:			lis       r3, -0x21
                addi      r3, r3, -4
                b         ret

label4:			lis       r3, -0x21
                addi      r3, r3, -3

ret:			lmw       r23, 0x4CC(r1)
                lwz       r0, 0x4F4(r1)
                mtspr   LR, r0
                addi      r1, r1, 0x4F0
                blr