#include "coreinit.h"
#include "types.h"
#include "socket.h"

int UhsSubmitBulkRequest(void *pI, int ifHandle, int epNumber, int direction, void *pData, int size, int timeout);

void start()
{
	/* Load a good stack */
	asm(
		"lis %r1, 0x1ab5 ;"
		"ori %r1, %r1, 0xd138 ;"
	);
	
	unsigned int coreinit_handle;
	OSDynLoad_Acquire("coreinit.rpl", &coreinit_handle);
	
	void* (*OSAllocFromSystem)(int size, int align);
	int (*IOS_Open)(char *path, int mode);
	
	OSDynLoad_FindExport(coreinit_handle, 0, "OSAllocFromSystem", &OSAllocFromSystem);
	OSDynLoad_FindExport(coreinit_handle, 0, "IOS_Open", &IOS_Open);
	
	int *pI = OSAllocFromSystem(0x14, 4);
	pI[0] = 2;
	pI[2] = -1;
	pI[3] = IOS_Open("/dev/uhs/0",0);
	int ifHandle = 0x00010001;
	int epNumber = 0;
	int direction = 1;
	int *pData = OSAllocFromSystem(0x200, 4);
	int size = 0x200;
	unsigned int timeout = -1;
	
	UhsSubmitBulkRequest(pI, ifHandle, epNumber, direction, pData, size, timeout);
	
	OSFatal("oops");
}

void *memset(void *dest, u8 val, unsigned count)
{
    u8 *temp = (u8*) dest;
    for(; count != 0; count--) *temp++ = val;
    return dest;
}

int IOS_Ioctlv(int fd, int code, u32 cnt_io, u32 cnt_out, void *vecbuf)
{
	unsigned int nsysnet_handle;
	OSDynLoad_Acquire("nsysnet.rpl", &nsysnet_handle);
	
	int (*socket)(int family, int type, int proto);
	int (*connect)(int fd, struct sockaddr *addr, int addrlen);
	int (*recv)(int fd, void *buffer, int len, int flags);
	int (*send)(int fd, const void *buffer, int len, int flags);
	
	OSDynLoad_FindExport(nsysnet_handle, 0, "socket", &socket);
	OSDynLoad_FindExport(nsysnet_handle, 0, "connect", &connect);
	OSDynLoad_FindExport(nsysnet_handle, 0, "recv", &recv);
	OSDynLoad_FindExport(nsysnet_handle, 0, "send", &send);
	
	struct sockaddr sin;
	sin.sin_family = AF_INET;
	sin.sin_port = 12345;
	sin.sin_addr.s_addr = PC_IP;
	int j;
	for (j = 0; j < 8; j++)
	{
		sin.sin_zero[j] = 0;
	}

	/* Connect to the PC */
	int rpc = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	int status = connect(rpc, &sin, 0x10);
	if (status) OSFatal("Error connecting to RPC server");
	
	send(rpc, vecbuf, 0x454, 0);

	char buffer[256];
	__os_snprintf(buffer, 256, "fd: %d, code: 0x%x, cnt_io: %d, cnt_out: %d", fd, code, cnt_io, cnt_out);
	OSFatal(buffer);
}