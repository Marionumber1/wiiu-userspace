if [ $# -ge 1 ]
then
    filename="${1%.*}"
else
    filename="test"
fi

if [ $# -ge 2 ]
then
	if [ $2 -eq "510" ]
	then
		ver="500"
	else
		ver="$2"
	fi
else
    ver="500"
fi

if [ ! -f findcode$ver.bin ]
then
    # Build findcode
    powerpc-eabi-gcc -nostdinc -fno-builtin -DVER=$ver -c ../src/findcode$ver.c #-Wa,-a,-ad
    powerpc-eabi-ld -Ttext 1800000 --oformat binary -o findcode$ver.bin findcode$ver.o  
    # Get rid of GCC function prologue and move stack out of our buffer
    # Result should start with: 94 21 E0 00 7C 3F 0B 78 3D 20 1D D7 61 29 B8 14
    dd if=findcode$ver.bin of=findcode$ver.bin bs=4 obs=4 skip=5 count=1 conv=notrunc
    dd if=findcode$ver.bin of=findcode$ver.bin bs=4 obs=4 skip=4 seek=1 count=1 conv=notrunc   
    dd if=findcode$ver.bin of=findcode$ver.bin bs=4 obs=4 skip=6 seek=2 conv=notrunc 
fi
    
#./stack_build.py $ver
if [ "$ver" == "400" ]
then
    dd if=findcode$ver.bin of=stack$ver.bin obs=352 seek=1 conv=notrunc
else
    dd if=findcode$ver.bin of=stack$ver.bin obs=432 seek=1 conv=notrunc
fi
powerpc-eabi-gcc -nostdinc -fno-builtin -DVER=$ver -c ../src/$filename.c #-Wa,-a,-ad      
powerpc-eabi-ld -Ttext 1800000 --oformat binary -o code.bin $filename.o uhs_asm.o

#$PHP stack.php $ver > $HTDOCS/test$ver.html
./generate_html.py $ver > $HTDOCS/$filename.html
